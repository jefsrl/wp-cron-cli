<?php

/*
Plugin Name: WP Cron Cli
Plugin URI: https://bitbucket.org/jefsrl/wp-cron-cli
Description: Access point to run crons from cli
Version: 0.0.10
Author: Jef Srl
Author URI: https://www.jef.it
*/
defined( 'ABSPATH' ) || exit;

$composerAutoloadPath = __DIR__ . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
if ( file_exists( $composerAutoloadPath ) ) {
	require_once $composerAutoloadPath;

	if ( class_exists( 'Puc_v4_Factory' ) ) {
		$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
			'https://bitbucket.org/jefsrl/wp-cron-cli',
			__FILE__,
			'wp-cron-cli'
		);

		$myUpdateChecker->setBranch('master');
	}
}