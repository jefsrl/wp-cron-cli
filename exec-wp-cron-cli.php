<?php

if(php_sapi_name() === 'cli'){
	ini_set('memory_limit','512MB');
	$data = implode('&', array_slice($argv, 1));
	parse_str($data, $_GET);
	parse_str($data, $_REQUEST);
	unset($data);
	if(isset($_GET['memory_limit'])) {
		ini_set('memory_limit',$_GET['memory_limit']);
	}
	chdir(dirname(__FILE__, 4));
	include dirname(__FILE__, 4).DIRECTORY_SEPARATOR.'wp-cron.php';
} else die();