# WP Cron Cli

A simple plugin ti trigger wp-cron for cli (to overcome webserver timeout).

## usage

run your command as
```
{{path to php bin}} {{wordpress root folder}}/wp-content/plugins/wp-cron-cli/exec-wp-cron-cli.php arg1=value arg2=value [....etc]
```